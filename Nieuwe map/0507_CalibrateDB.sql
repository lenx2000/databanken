USE Modernways;
CREATE TABLE Metingen (
Tijdstip DATETIME,
Groote SMALLINT UNSIGNED,
Marge DOUBLE(3,2)
);